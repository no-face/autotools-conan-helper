from conans import ConanFile

class AutotoolsHelperConan(ConanFile):
    name         = "AutotoolsHelper"
    version      = "0.1.0"
    description  = "A python class to help build conan Autotools projects."
    url          = "https://gitlab.com/no-face/autotools-conan-helper.git"
    license      = "unlicense"
    exports      = '*.py'
    build_policy = "missing"

    def package(self):
        self.copy('*.py')

    def package_info(self):
        self.env_info.PYTHONPATH.append(self.package_folder)

        self.cpp_info.includedirs = []
        self.cpp_info.libdirs = []
        self.cpp_info.resdirs = []
