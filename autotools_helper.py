#!/usr/bin/env python
# -*- coding: utf-8 -*-

import platform
from os import path

from conans import tools, AutoToolsBuildEnvironment


class DelegateProperty(object):

    def __init__(self, delegate_name, prop_name):
        self.delegate_name = delegate_name
        self.prop_name = prop_name

    def __get__(self, instance, clazz):
        return getattr(self.delegate(instance), self.prop_name)

    def __set__(self, instance, value):
        return setattr(self.delegate(instance), self.prop_name, value)

    def delegate(self, instance):
        return getattr(instance, self.delegate_name, None)

class Autotools(object):
    def __init__(self, conanfile, **kwargs):
        self.conanfile    = conanfile
        self.shared       = kwargs.get('shared', None)
        self.options      = {}
        self.with_options = {}
        self.args         = []

        self.install_dir    = kwargs.get('install_dir', conanfile.package_folder)
        self.configure_dir  = kwargs.get('configure_dir', '.')

        self.env_build = AutoToolsBuildEnvironment(conanfile)


    ################################### Properties ############################################

    fpic          = DelegateProperty('env_build', 'fpic')
    libs          = DelegateProperty('env_build', 'libs')
    include_paths = DelegateProperty('env_build', 'include_paths')
    library_paths = DelegateProperty('env_build', 'library_paths')
    defines       = DelegateProperty('env_build', 'defines')
    flags         = DelegateProperty('env_build', 'flags')
    cxx_flags     = DelegateProperty('env_build', 'cxx_flags')
    link_flags    = DelegateProperty('env_build', 'link_flags')

    ################################### Methods ############################################

    def configure(self, build=None, host=None, target=None):
        options = self.make_configure_options()

        self.env_build.configure(self.configure_dir, args=options, build=build, host=host, target=target)

    def build(self):
        # self.exec_cmd("make")
        self.env_build.make()

    def install(self):
        self.env_build.make(args=["install"])

    def add_arg(self, arg):
        self.args.append(arg)

    def enable(self, feature):
        self.set_enabled(feature, True)

    def disable(self, feature):
        self.set_enabled(feature, False)

    def set_enabled(self, feature, value):
        self.options[feature] = value

    def with_feature(self, feature, value=True):
        self.with_options[feature] = value

    def without_feature(self, feature):
        self.with_feature(feature, None)

    def set_with(self, feature, value):
        if value is None or value is False:
            self.without_feature(feature)
        else:
            self.with_feature(feature, value)

    ################################### Helpers ###############################################

    def exec_cmd(self, cmd, **dict_args):
        with tools.environment_append(self.env_build.vars):
             self.print_and_run(cmd)

    def print_and_run(self, cmd, **dict_args):
        self.conanfile.output.info(cmd)

        if tools.os_info.is_windows:
            tools.run_in_windows_bash(self, cmd, **dict_args)
        else:
            self.run(cmd, **dict_args)

    def run(self, *args, **dict_args):
        self.conanfile.run(*args, **dict_args)

    def make_configure_options(self):
        opts = list(self.args)
        opts.append("--prefix={}".format(self.install_dir))

        if self.shared is not None:
            opts.append(self.autotools_bool_option("shared", self.shared))
            opts.append(self.autotools_bool_option("static", not self.shared))

        for optname, optvalue in self.options.items():
            opts.append(self.autotools_bool_option(optname, optvalue))

        for feat, value in self.with_options.items(): # inefficient on Py2
            opts.append(self.autotools_with_option(feat, value))

        #return " ".join(opts)
        return opts

    def autotools_bool_option(self, option_base_name, value):
        prefix = "--enable-" if value else "--disable-"

        return prefix + option_base_name

    def autotools_with_option(self, feat, value):
        prefix = "--without" if value is None else "--with"

        opt = "{}-{}".format(prefix, feat)

        if not value is None and value is not True:
            opt += "={}".format(value)

        return opt
