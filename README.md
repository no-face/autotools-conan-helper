# Autotools Helper

Simple helper for build [conan](https://www.conan.io/) packages that uses autotools.


## How to use

Include this package on your build.


```python
requires = "AutotoolsHelper/0.0.1@noface/experimental"
```

Then, in your build:

```python
from conans.tools import pythonpath

...

def build(self):
    ...
    with pythonpath(self):
        from autotools_helper import Autotools

        autot = Autotools(
            # (required) pass conanfile instance
            self,

            # (optional) defaults to conanfile.package_folder. Adds '--prefix=<install_dir>'
            install_dir = "install_dir"

            # (optional) defines a shared or static build, if not set uses autotools default
            shared      = self.options.shared)

        # enable/disable options (optional)
        autot.enable("abc")  # adds option --enable-abc
        autot.disable("xyz") # adds option --disable-xyz

        # set features (optional)
        autot.with_feature("feature", "value")  # adds --with-feature=value
        autot.without_feature("other")          # adds --without-other

        # run autotools commands
        autot.configure()       # ./configure <options>
        autot.build()           # make
        autot.install()         # make install

...

def package(self):
    # if Autotools.install_dir was set to 'conanfile.package_folder' (default), the files will be automatically installed
    pass
```
